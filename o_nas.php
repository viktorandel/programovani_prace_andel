<!DOCTYPE html>
<html lang="cs">
<head>
	<title>O nás</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="o_nas.css">
</head>
<body>
<header>
	<img class="logo" src="logo.png" width="180" height="160" alt="logo">
	<div class="nav">
	<?php include('index_menu2.php'); ?>
</div>
	</header>
	<h1>O NÁS...</h1>
	<p class="uvodni">Jsme špičkovou čistírnou oblečení s dlouholetou tradicí. Své služby poskytujeme hlavně v Jihomoravském kraji a okolí. Nicméně jsme již započali plány s výstavbou naší nové prodejny v Praze. Brzy tak budeme moci poskytovat své služby i obyvatelům hlavního města a Středočeského kraje. </p>
	<p class="text_uvod">Naše služby si můžete pohodlně objednat z domova přes náš formulář v záložce ,,Objednávky". Jakmile bude objednávka potvrzena, ihned se za vámi vydá náš kurýr, aby si od vás prádlo vyzvednul. V našem ceníku se pak mužete podívat, jak dlouho vyprání vašeho typu oblečení bude trvat. Hned po vyprání a <b>vysušení (které je v ceně)</b> vašeho oblečení, se za vámi náš kurýr vydá.</p>
	<p class="text_uvodni">Platit můžete buďto online při vyřizování své objednávky na našem webu, nebo osobně našemu kurýrovi v momentě kdy si od vás bude objednávku přebírat. Je zde také možnost donést nám vaše oblečení osobně na naší prodejnu na Jičínského náměstí 34/9 v Brně. <b>Máme pro vás otevřeno každý den od 9:00 do 20:00.</b> </p>
	<img class="probraz" src="o_nas1.jpeg" width="250" height="200" alt="interierraminka">
	<img class="dobraz" src="o_nas2.jpg" width="250" height="200" alt="interierpracka">
	<?php include('index_footer.php'); ?>
</body>
</html>