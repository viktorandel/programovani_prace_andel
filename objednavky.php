<!DOCTYPE html>
<html lang="cs">
<head>
	<title>Objednávky</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="objednavky.css">
</head>
<body>
<header>
	<img class="logo" src="logo.png" width="180" height="160" alt="logo">
	<div class="nav">
	<?php include('index_menu4.php'); ?>
	</div>
	<h1>OBJEDNÁVKY</h1>
	<p class="uvodni">Pro zpracování vaší ovjednávky prosím vyplňte tento formulář. Prosíme abyste uvedli přesný počet kusů vašeho oblečení a také správný materiál. Vlastníte-li materiál který není v nabídce uveden, prosím napište to do kolonky <b>,,Poznámky"</b>. Pokud žádáte o expresní vyřízení vaší objednávky také to prosím vepište do poznámek. Po dokončení vaší objednávky vás budeme kontaktovat abychom se dohodli na podrobnostech. Jakmile dokočíte vaší objednávku, příjdou Vám na váš e-mail a telefonnní číslo, které jste zadali při registraci, všechny potřebné informace. </p>

	<form>
		<input class="jmeno" type="text" placeholder="Jméno a příjmení" name="Jméno a příjmení">
		<input class="email" type="text" placeholder="E-mail" name="E-mail">
		<input class="telefon"  type="text"  placeholder="Telefonní číslo" name="Telefonní číslo">
		<input class="adresa" type="text" placeholder="Adresa" name="Adresa">
		<input class="mesto" type="text" placeholder="Město" name="Město">
		<input class="psc" type="text" placeholder="PSČ" name="PSČ">
		<select class="prvni" name="druh">
			<option value="oo">Obyčejné oblečení</option>
			<option value="oo">Bavlna</option>
			<option value="oo">Samet</option>
			<option value="oo">Ložní prádlo</option>
			<option value="oo">Zimní bundy</option>
			<option value="oo">Kůže</option>
			<option value="oo">Latex</option>
			<option value="oo">Koženka</option>
			<option value="oo">Potahy</option>
			<option value="oo">Deky</option>
		</select>
		<select class="druhy" name="druh">
			<option value="oo">Obyčejné oblečení</option>
			<option value="oo">Bavlna</option>
			<option value="oo">Samet</option>
			<option value="oo">Ložní prádlo</option>
			<option value="oo">Zimní bundy</option>
			<option value="oo">Kůže</option>
			<option value="oo">Latex</option>
			<option value="oo">Koženka</option>
			<option value="oo">Potahy</option>
			<option value="oo">Deky</option>
		</select>
		<input class="pocet" type="number" placeholder="Počet kusů" name="Počet kusů">
		<input class="pocet_druhy" type="number" placeholder="Počet kusů" name="Počet kusů">
		<textarea class="poznamky" name="Poznámky" placeholder="Poznámky..." cols="23" rows="5">
</textarea>	
	<select class="platba" name="platba">
			<option value="oo">Platební kartou</option>
			<option value="oo">Osobně</option>
		</select>
		<input class="objednat" type="submit" value="Dokončit objednávku">

	</form>

	</header>
	<?php include('index_footer.php'); ?>
</body>
</html>