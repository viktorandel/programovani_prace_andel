<!DOCTYPE html>
<html lang="cs">
<head>
	<title>Ceník</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="cenik.css">
</head>
<body>
<header>
	<img class="logo" src="logo.png" width="180" height="160" alt="logo">
	<div class="nav">
	<?php include('index_menu3.php'); ?>
	</div>
	<h1>CENÍK</h1>
	<p class="uvodnik">Ceny v našem ceníku se liší podle druhu oblečení. <b>U obyčejného oblečení a dek a potahů a ložního prádla je cena rovna jednomu kilogramu oblečení. U ostatních speciálních materiálů je cena rovna jednomu kusu oblečení. </b> Expresní služba znamená že vaše oblečení bude mít vyřizovací prioritu, jsme tedy pak schopni na základě telefonické dohody dodat vaše oblečení co nejdříve, nejlépe ve Vámi stanovenou dobu.</p>
		<table class="cenik">
			<tr>
	<th>Služba</th>
	<th>Cena</th>
	<th>Doba vyprání</th>
</tr>
<tr>
	<td class="nazev">Obyčejné oblečení</td>
	<td>90,- Kč</td>
	<td>30 minut</td>
</tr>
<tr>
	<td class="nazev">Bavlna</td><td>110,- Kč</td><td>90 minut</td>
</tr>
<tr>
	<td class="nazev">Samet</td><td>250,- Kč</td><td>120 minut
</tr>
<tr>
	<td class="nazev">Ložní prádlo</td><td>350,- Kč</td><td>120 minut</td>
</tr>
<tr>
	<td class="nazev">Zimní bundy</td><td>400,- Kč</td><td>90 minut</td>
</tr>
<tr>
	<td class="nazev">Kůže</td><td>600,- Kč</td><td>120 minut</td>
</tr>
<tr>
	<td class="nazev">Latex</td><td>500,- Kč</td><td>120 minut</td>
</tr>
<tr>
	<td class="nazev">Koženka</td><td>200,- Kč</td><td>90 minut</td>
</tr>
<tr>
	<td class="nazev">Potahy</td><td>200,- Kč</td><td>120 minut</td>
</tr>
<tr>
	<td class="nazev"> Deky</td><td>225,- Kč</td><td>120 minut</td>
</tr>
<tr>
	<td class="nazev">Expresní zásilka</td><td>150,- Kč</td>
</tr>
</table>
	</header>

	<?php include('index_footer.php'); ?>
</body>
</html>